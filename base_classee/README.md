Ce répertoire contient une base `monavenir.db` dont les candidatures ont été classées, l'option est disponible dans l'interface du profil `admin`. 

Le code de la fonction de classement est donné ci-dessous. L'exécution est longue (1 minute environ).s

```python

#controleur de route / URL
@app.route('/classement')
def classement():
    """Fonction de classement des candidatures
    Temps d'exécution long ! 
    """
    conn = sqlite3.connect('monavenir.db')        
    cur = conn.cursor()
    #on récupère tous les idSuperieur des établissements du supérieur
    cur.execute("SELECT idSuperieur FROM superieur ;")
    #pour chaque établissement
    liste_idSuperieur = cur.fetchall()
    #ici on a récupéré une liste de tuple
    requeteClassement = """
        SELECT candidature.idEleve AS idAttente, (note1 * coefNote1 + note2 * coefNote2)/(coefNote1 + coefNote2) AS moyenne
            FROM (superieur JOIN candidature USING(idSuperieur)) JOIN eleve USING(idEleve) 
                WHERE idSuperieur = ?
                    ORDER BY moyenne DESC
                        LIMIT  ? ;
            """
    for etab in liste_idSuperieur:        
        idSuperieur = int(etab[0])
        #on récupère les quotas d'admis et de file d'attente
        cur.execute("SELECT nbAdmis, nbAppel FROM superieur WHERE idSuperieur = ?;", (idSuperieur,))
        quotas = cur.fetchone()
        nbAdmis = int(quotas[0])
        nbAppel = int(quotas[1])
        #on fixe le statut des en attente        
        cur.execute(requeteClassement, (idSuperieur,  nbAppel))
        liste_attente = cur.fetchall()
        #on fixe le statut des candidats dans la liste d'appel
        for k in range(nbAppel):
            if k < nbAdmis: 
                cur.execute("UPDATE candidature SET statut = 'admis' WHERE idEleve = ? AND idSuperieur = ? ;", (liste_attente[k][0],idSuperieur))
            else:
                cur.execute("UPDATE candidature SET statut = 'enAttente' WHERE idEleve = ? AND idSuperieur = ? ;", (liste_attente[k][0],idSuperieur))
        #on enregistre les modifs précédentes
        conn.commit()
        #enfin on fixe le statut des refusés
        cur.execute("UPDATE candidature SET statut = 'refuse' WHERE idSuperieur = ? AND statut = 'nonTraite';", (idSuperieur,))
        #on enregistre 
        conn.commit()
    return redirect(url_for('interface')) #redirection vers l'URL gérée par interface
```