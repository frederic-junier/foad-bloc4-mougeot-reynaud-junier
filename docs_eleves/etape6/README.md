Ce répertoire contient trois sous-répertoires correspondant à trois ateliers pour le TP noté de l'étape 6 de notre projet.

* `ateliers_eleve` contient l'énoncé, le matériel et les corrrigés pour l'atelier sur le profil `eleve`.
* `ateliers_admin` contient l'énoncé, le matériel et les corrrigés pour l'atelier sur le profil `admin`.
* `ateliers_superieur` contient l'énoncé, le matériel et les corrrigés pour l'atelier sur le profil `superieur`,
c'est le seul atelier où les élèves travaillent sur une base dont les candidatures ont été classées. 
Attention lorsqu'on classe la base, elle ne doit pas être ouverte dans un autre logiciel. 