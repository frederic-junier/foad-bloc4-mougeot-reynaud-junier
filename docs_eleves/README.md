Ce répertoire contient :

* Le fichier de synthèse de notre projet  aux formats `pdf` : `MonAvenir.pdf`   et `docx` : `MonAvenir.docx`.
* Un sous-répertoire `etape4` avec toutes les ressources pour l'étape 4 de notre projet _Découverte de la programmation Web côté serveur avec Falsk_.
* Un sous-répertoire `etape6` avec toutes les ressources pour l'étape 6 de notre projet _TP noté sur les formulaires Web et la programmation d'application Web avec base de données_.
* Une archive `materiel.zip` avec tout le matériel élève pour les différentes étapes


