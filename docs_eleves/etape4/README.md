Ce répertoire contient les fichiers d'énoncé et le matériel pour l'étape 4 de notre projet : _Découverte de  la programmation Web côté serveur avec Flask_.

* Les fichiers `etape4.*` à la racine du répertoire contiennent l'énoncé de l'aactivité sous différents formats. Un fichier Makefile permet de les générer avec la commande `make` à partir de la source `etape4.md` qui est au format Markdown.
* Le dossier `materiel` contient les différents fichiers fournis aux élèves. L'archive `materiel.zip` rassemble le contenu de `materiel` dans un seul fichier.
* L'archive `materiel-corrige.zip` contient les corrigés.